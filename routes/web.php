<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Message;
use Illuminate\Support\Facades\Artisan;

Route::get('/clear_cache', function() {
    Artisan::call('cache:clear');
});
Route::get('/clear_route', function() {
    Artisan::call('route:clear');
});

Route::get('/view_route', function() {
    Artisan::call('view:clear');
});

Route::get('/savanna1/down', function() {
    Artisan::call('down');
});

Route::get('/savanna1/up', function() {
    Artisan::call('up');
});

Route::get('/', 'PagesController@home')->name('home');
Route::get('/board', 'PagesController@board')->name('board');
Route::get('/team', 'PagesController@team')->name('team');
Route::get('/partners', 'PagesController@partners')->name('partners');
Route::get('/contact', 'PagesController@contact')->name('contact');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/events', 'PagesController@events')->name('events');
Route::get('/publications', 'PagesController@publications')->name('publications');
Route::get('/publication/{id}', 'PagesController@publicationDownload')->name('publication.download');

Route::get('/diplomacy', 'PagesController@diplomacy')->name('diplomacy');
Route::get('/development', 'PagesController@development')->name('development');
Route::get('/democracy', 'PagesController@democracy')->name('democracy');

Route::get('/tools/{tool}', 'PagesController@tool')->name('tool');
Route::get('/tools/meetings/{meeting}', 'PagesController@meeting')->name('meeting');



// Route::get('/projects', 'PagesController@projects')->name('projects');
Route::get('/multimedia', 'PagesController@media')->name('media');
Route::get('/what-we-do', 'PagesController@whatWeDo')->name('whatWeDo');
Route::post('/subscribe', 'PagesController@subscribe')->name('subscribe');
Route::post('send-message', 'MessageController@sendMessage')->name('message.send');
Route::post('vote', 'PollController@vote')->name('poll.vote');

/*
 the admin routes
*/

Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    Route::get('/dashboard' , 'AdminController@index')->name('admin.dashboard');
    Route::post('/add-admin' , 'AdminController@addAdmin')->name('admin.create');
    Route::get('/delete-admin/{id}' , 'AdminController@delAdmin')->name('admin.delete');

    //edit
    Route::get('/home-page' , 'HomeController@homepage')->name('home.page');
    Route::post('update-about' , 'HomeController@updateAbout')->name('about.update');

    Route::get('/mailing-list', 'MailingController@index')->name('mailin.list');

    Route::get('/board-page' , 'BoardController@boardpage')->name('board.page');
    Route::post('create-board' , 'BoardController@createBoard')->name('board.create');
    Route::post('edit-board/{id}' , 'BoardController@editBoard')->name('board.edit');
    Route::get('feature-board-member/{id}' , 'BoardController@featureBoard')->name('board.feature');
    Route::get('delete-board-member/{id}' , 'BoardController@deleteBoard')->name('board.delete');


    // for team
    Route::get('/team-page' , 'TeamController@teampage')->name('team.page');
    Route::post('create-team' , 'TeamController@createteam')->name('team.create');
    Route::post('edit-team/{id}' , 'TeamController@editteam')->name('team.edit');
    Route::get('feature-team-member/{id}' , 'TeamController@featureteam')->name('team.feature');
    Route::get('delete-team-member/{id}' , 'TeamController@deleteteam')->name('team.delete');

    Route::get('/partners-page' , 'PartnerController@partnerspage')->name('partners.page');
    Route::post('create-partner' , 'PartnerController@createPartner')->name('partner.create');
    Route::post('edit-partner/{id}' , 'PartnerController@editPartner')->name('partner.edit');
    Route::get('feature-partner/{id}' , 'PartnerController@featurepartner')->name('partner.feature');
    Route::get('delete-partner/{id}' , 'PartnerController@deletepartner')->name('partner.delete');

    Route::get('/publications-page' , 'PublicationController@publicationspage')->name('publications.page');
    Route::post('create-publication' , 'PublicationController@createPublication')->name('publication.create');
    Route::post('edit-publication/{id}' , 'PublicationController@editPublication')->name('publication.edit');
    Route::get('feature-publication/{id}' , 'PublicationController@featurePublication')->name('publication.feature');
    Route::get('delete-publication/{id}' , 'PublicationController@deletePublication')->name('publication.delete');

    Route::get('/events-page' , 'EventController@eventsPage')->name('events.page');
    Route::post('create-event' , 'EventController@createEvent')->name('event.create');
    Route::post('edit-event/{id}' , 'EventController@editEvent')->name('event.edit');
    Route::get('feature-event/{id}' , 'EventController@featureEvent')->name('event.feature');
    Route::get('delete-event/{id}' , 'EventController@deleteEvent')->name('event.delete');

    Route::get('/polls-page' , 'PollController@index')->name('polls.page');
    Route::post('create-poll' , 'PollController@store')->name('poll.create');
    Route::get('feature-poll/{poll}' , 'PollController@feature')->name('poll.feature');
    Route::get('unfeature-poll/{poll}' , 'PollController@unfeature')->name('poll.unfeature');
    Route::get('delete-poll/{poll}' , 'PollController@destroy')->name('poll.delete');


    Route::get('/tools-page' , 'ToolController@index')->name('tools.page');
    Route::post('create-tool' , 'ToolController@store')->name('tool.create');
    Route::post('edit-tool/{id}' , 'toolController@update')->name('tool.edit');
    Route::get('feature-tool/{tool}' , 'ToolController@feature')->name('tool.feature');
    Route::get('unfeature-tool/{tool}' , 'ToolController@unfeature')->name('tool.unfeature');
    Route::get('delete-tool/{tool}' , 'ToolController@destroy')->name('tool.delete');

    Route::get('/meetings-page' , 'meetingController@index')->name('meetings.page');
    Route::get('/meetings/{meeting}' , 'meetingController@show')->name('meeting.show');
    Route::post('create-meeting' , 'meetingController@store')->name('meeting.create');
    Route::post('edit-meeting/{id}' , 'meetingController@update')->name('meeting.edit');
    Route::get('feature-meeting/{id}' , 'meetingController@feature')->name('meeting.feature');
    Route::get('delete-meeting/{id}' , 'meetingController@destroy')->name('meeting.delete');

    Route::post('create-photo/{meeting}' , 'PhotoController@store')->name('photo.create');
    Route::get('feature-photo/{photo}' , 'PhotoController@feature')->name('meeting.photo.feature');
    Route::get('delete-meeting-photo/{photo}' , 'PhotoController@destroy')->name('meeting.photo.destroy');


    Route::get('/multimedia-page' , 'MultimediaController@multimediapage')->name('multimedia.page');
    Route::post('add-image-gallry' , 'MultimediaController@imageUpload')->name('image.upload');
    Route::get('feature-photo/{id}', 'MultimediaController@photoFeature')->name('photo.feature');
    Route::get('delete-photo/{id}', 'MultimediaController@photoDelete')->name('photo.delete');


    Route::get('/contact-page' , 'ContactController@contactpage')->name('contact.page');
    Route::post('contact-update', 'ContactController@updateContact')->name('contact.update');

    Route::get('/program-page' , 'ProgramController@programpage')->name('program.page');



    Route::post('/create-slide/{page}' , 'SlideController@addSlide')->name('slide.create');
    Route::get('feature/{page}/{id}', 'SlideController@feature')->name('feature');
    Route::post('edit-slide/{page}/{id}', 'SlideController@editSlide')->name('slide.edit');
    Route::get('delete-slide/{page}/{id}', 'SlideController@deleteSlide')->name('slide.delete');


    Route::get('mark-as-read/{id}', 'MessageController@markAsRead')->name('message.read');
    Route::get('message/{id}', 'MessageController@messageShow')->name('message.show');
    Route::get('delete-message/{id}', 'MessageController@messageDelete')->name('message.delete');
    Route::get('message', 'MessageController@messageIndex')->name('message.all');
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('/login' , 'SessionController@showAdminForm')->name('admin.login');
    Route::post('/login' , 'SessionController@adminLogin')->name('admin.login.submit');
    Route::get('/logout' , 'SessionController@adminLogout')->name('admin.logout');
});
