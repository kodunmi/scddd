<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['description','image','feature','meeting_id'];

    public function meeting()
    {
        return $this->belongsTo(Meeting::class);
    }

}
