<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $meeting)
    {

        $dis = $request->description;
        $img = $request->image;

        for($count = 0; $count < count($img); $count++){
            $imageName[] = Str::random(10).'.'.$img[$count]->getClientOriginalExtension();
            $img[$count]->move(public_path('images/meetings'), $imageName[$count]);

            $data = array(
                'image' => $imageName[$count],
                'description' => $dis[$count],
                'meeting_id' => $meeting
            );

            $insert_data[] = $data;
        }

        Photo::insert($insert_data);

        return back()->with([
            'message' => 'image submitted',
            'type' => 'success'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy($photo)
    {
        $photo = Photo::find($photo);
        $file = public_path().'/images/meetings/'.$photo->first()->image;

        File::delete($file);
        $photo->delete();

        return back()->with([
            'message' => 'photo deleted',
            'type' => 'success'
        ]);
    }

    public function feature($photo)
    {
        $photo = Photo::find($photo);

        if($photo->first()->feature){
            $photo->update(['feature' => false]);

            return back()->with([
                'message' => 'photo removed from feature',
                'type' => 'success'
            ]);
        }else{
            $photo->update(['feature' => true]);

            return back()->with([
                'message' => 'photo featured ',
                'type' => 'success'
            ]);
        }
    }
}
