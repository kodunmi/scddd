<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class BoardController extends Controller
{
    public function boardpage(){
        return view('admin.pages.edits.board', [
            'slides' => DB::table('board_slider')->get(),
            'boards' => DB::table('boards')->get()
        ]);
    }

    public function createBoard(Request $request){

        $this->validate($request,[
            'name' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg',
            'position' => 'required',
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/board'), $imageName);

        $feature = $request->feature ? true : false;

        $data = $request->except(['_token']);

        $data['image'] = $imageName;
        $data['feature'] = $feature;

        DB::table('boards')->insert($data);

        return back()->with([
            'message' => 'updated successfully',
            'type' => 'success'
        ]);
    }

    public function editBoard(Request $request, $id){
        $member = DB::table('boards')->where('id',$id);

        $data = $request->except(['_token']);

        if ($request->has('image')) {
            $file = public_path().'/images/board/'.$member->first()->image;
            File::delete($file);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/board'), $imageName);

            $data['image'] = $imageName;
        }

        $member->update($data);

        return back()->with([
            'message' => 'board updated',
            'type' => 'success'
        ]);
    }

    public function featureBoard($id){

        $member = DB::table('boards')->where('id',$id);

        if($member->first()->feature){
            $member->update(['feature' => false]);

            return back()->with([
                'message' => 'member removed from feature',
                'type' => 'success'
            ]);
        }else{
            $member->update(['feature' => true]);

            return back()->with([
                'message' => 'member featured ',
                'type' => 'success'
            ]);
        }
    }

    public function deleteBoard($id){

        $member = DB::table('boards')->where('id',$id);
        $file = public_path().'/images/board/'.$member->first()->image;


        File::delete($file);
        // if(File::exists($file)){
        //     File::delete($file);
        // }
        $member->delete();

        return back()->with([
            'message' => 'mem$member deleted',
            'type' => 'success'
        ]);
    }
}
