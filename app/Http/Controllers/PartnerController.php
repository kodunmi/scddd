<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PartnerController extends Controller
{
    public function partnerspage(){
        return view('admin.pages.edits.partners', [
            'slides' => DB::table('partners_slide')->get(),
            'partners' => DB::table('partners')->get()
        ]);
    }

    public function createpartner(Request $request){

        $this->validate($request,[
            'name' => 'required',
            'image' => 'required|image|mimes:png,jpg,jpeg',
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images/partner'), $imageName);

        $feature = $request->feature ? true : false;

        $data = $request->except(['_token']);

        $data['image'] = $imageName;
        $data['feature'] = $feature;

        DB::table('partners')->insert($data);

        return back()->with([
            'message' => 'updated successfully',
            'type' => 'success'
        ]);
    }

    public function editPartner(Request $request,$id){

        $partner = DB::table('partners')->where('id',$id);

        $data = $request->except(['_token']);

        if ($request->has('image')) {
            $file = public_path().'/images/partner/'.$partner->first()->image;
            File::delete($file);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images/partner'), $imageName);

            $data['image'] = $imageName;
        }

        $partner->update($data);

        return back()->with([
            'message' => 'partner updated',
            'type' => 'success'
        ]);
    }
    public function featurepartner($id){

        $partner = DB::table('partners')->where('id',$id);

        if($partner->first()->feature){
            $partner->update(['feature' => false]);

            return back()->with([
                'message' => 'partner removed from feature',
                'type' => 'success'
            ]);
        }else{
            $partner->update(['feature' => true]);

            return back()->with([
                'message' => 'partner featured ',
                'type' => 'success'
            ]);
        }
    }

    public function deletepartner($id){

        $partner = DB::table('partners')->where('id',$id);
        $file = public_path().'/images/partner/'.$partner->first()->image;


        File::delete($file);
        // if(File::exists($file)){
        //     File::delete($file);
        // }
        $partner->delete();

        return back()->with([
            'message' => 'partner deleted',
            'type' => 'success'
        ]);
    }
}
